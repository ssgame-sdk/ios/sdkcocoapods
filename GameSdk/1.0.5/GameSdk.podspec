license = <<-EOF
Copyright (c) 2018, Baioo Limited.
All rights reserved.
This sources and libs can only be used in BAIOO Family Interactive Limited and its subsidiaries.
EOF

Pod::Spec.new do |s|

  s.name         = "GameSdk"
  s.version     = "1.0.5"
  s.summary      = "GameSdk的CocoaPods"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = <<-DESC
  			  GameSdk的CocoaPods
                   DESC

  s.homepage     = "https://gitlab.xf/sdk-client/gamesdk/gamesdk-ios.git"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  s.license      = { :type => "Ssgame", :text => license }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.author             = { "tanjiatao" => "tanjiatao@xf727.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  s.platform     = :ios, "9.0"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :http => "http://mvn.sl916.com/repository/maven-cocoapods/#{s.name}/#{s.version}/#{s.name}.zip", :flatten => true}

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  # ――― sub spec ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.subspec 'SsgameCommonSdk' do |c|

    c.vendored_frameworks = 'SsgameCommonSdk.framework'

  end

  s.subspec 'SsgameSenSorsSdk' do |sa|

    sa.vendored_frameworks = 'SsgameSenSorsSdk.framework'
    sa.libraries = 'icucore', 'z', 'c++', 'sqlite3'

  end

  s.subspec 'SsgameDataTrack' do |da|

    da.vendored_frameworks = 'SsgameDataTrack.framework'
    da.dependency 'GameSdk/SsgameSenSorsSdk'

  end
 
  s.subspec 'StarApiSdk' do |st|

    st.vendored_frameworks = 'StarApiSdk.framework'
    st.dependency 'GameSdk/SsgameCommonSdk'

  end

  s.subspec 'ShenlanUISdk' do |su|

      su.vendored_frameworks = 'ShenlanUISdk.framework'
      su.dependency 'GameSdk/SsgameCommonSdk'
      su.dependency 'GameSdk/StarApiSdk'
      su.resources = 'ShenlanUIBundle.bundle'
        
  end

  s.subspec 'SsgameFrameSdk' do |sf|

    sf.vendored_frameworks = 'SsgameFrameSdk.framework'
    sf.frameworks = 'SystemConfiguration', 'CFNetwork', 'StoreKit', 'AdSupport', 'JavaScriptCore', 'ImageIO', 'iAd', 'SafariServices', 'CoreTelephony', 'CoreText', 'CoreGraphics', 'CoreImage', 'UIKit'
    sf.dependency 'GameSdk/SsgameCommonSdk'
    sf.dependency 'GameSdk/StarApiSdk'
    sf.dependency 'GameSdk/ShenlanUISdk'
    sf.dependency 'GameSdk/SsgameDataTrack'
    sf.dependency 'GameSdk/SsgameSenSorsSdk'
    sf.resources = 'ShenlanUIBundle.bundle'
    
  end

  #s.subspec 'SdkUnityFramework' do |ui|
  #
  # ui.vendored_frameworks = 'SdkUnityFramework.framework'
  #
  #end


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  s.requires_arc = true

end
